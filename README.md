# Little example of Chat system using websockets and NodeJS as server

## Requirements
- NodeJS
- PHP 5.4+


## Installation

 1. Open Terminal
 2. Clone the repo
 3. install websocket module

```
npm install websocket
```

 4. Launch node server

```
node myserver.js
```

 5. Serve your directory as website

```
php -S localhost:8000
```

 6. Open two different browsers http://localhost:8000
 7. enjoy !

## References

- [https://www.npmjs.com/package/websocket](https://www.npmjs.com/package/websocket)
- [http://book.mixu.net/node/ch10.html](http://book.mixu.net/node/ch10.html)
- [https://github.com/theturtle32/WebSocket-Node/tree/master/docs](https://github.com/theturtle32/WebSocket-Node/tree/master/docs)

----------

Written by Stephane Chapdelaine Sept 2015
