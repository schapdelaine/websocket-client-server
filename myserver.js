/**
* Node Server for websockets
* @author Stephane Chapdelaine <stephanechapdelaine@gmail.com>
* @see https://github.com/theturtle32/WebSocket-Node/tree/master/docs for informations about WebSocket classes
*/

// Standard node module
var http = require('http');
// Additional module to install with npm
var WebSocketServer = require('websocket').server;

// Create a server http server
var server = http.createServer(function(request, response) {
    console.log((new Date()) + ' Received request for ' + request.url);
    response.writeHead(404);
    response.end();
});

// Set the server to listen on port 54321
server.listen(54321, function() {
    console.log((new Date()) + ' Server is listening on port 54321');
});

// Hold the clients that will connect
var clients = [];

// Initialize the websocket server using the http server
var socket = new WebSocketServer({
    httpServer: server,
    autoAcceptConnections: false
});

// Handle the request made to the websocket server
socket.on('request', function(request) {
    debug('Receiving request from IP: ' + request.remoteAddress);
    debug('Request ressource: ' + request.resource);
    var client_id = null;
    // The client should first request /ident/ID where ID is an unique value
    // So we parse the url called to get that ID
    var resource_parts = request.resource.split('/');
    if (resource_parts[1] && resource_parts[1] === 'ident' && resource_parts[2]) {
        // Found the ID
        client_id = resource_parts[2];
        // Accepting the connection if the right protocol is supplied since the request was well formatted
        var connection = request.accept('client-notification', request.origin);
        // If the connection is done we proceed
        if (connection && connection.connected) {
            // saving the ID of the client to the connection
            connection.client_id = client_id;
            // Adding the new client to our list of clients to iterate on broadcast messages
            clients.push(connection);
            // This is where we handle the actions to do with the data received
            connection.on('message', function(message) {
                if (message) {
                    debug('Receiving message: '+message.utf8Data);
                    var command = JSON.parse(message.utf8Data);
                    if (command.action) {
                        debug('Action Broadcast Requested!');
                        switch (command.action) {
                            case 'broadcast':
                               action_broadcast(connection,command);
                               break;
                            default:
                                break;
                        }
                    }
                }
            });
            // Let know other clients that a new user has connected
            action_newclient(connection);
        }
    }
});

/**
 * Handler when a socket/client lost his connection with the server
 */
socket.on('close', function(connection, closeReason, description) {
    console.log("Lost the client_id "+connection.client_id+" Reason("+closeReason+")");
});

/**
 * @see https://github.com/theturtle32/WebSocket-Node/blob/master/docs/WebSocketConnection.md
 * @param  {WebSocketConnection} connection
 * @return {void}
 */
function action_newclient(connection) {
    action_broadcast(connection,{
        "datahandler":"clientconnect",
        "data":""
    });
}

/**
 * Allow to broadcast data to all clients connected except the source
 * @param  {WebSocketConnection} fromConnection
 * @param  {JSON Object} message
 * @return {void}
 */
function action_broadcast(fromConnection,message) {
    message.client_id = fromConnection.client_id;
    console.log('broadcast',JSON.stringify(message));
    clients.forEach(function(connection) {
        if (connection.connected && connection.client_id != fromConnection.client_id) {
            debug('Broadcasting to client_id ' + connection.client_id + JSON.stringify(message));
            connection.send(JSON.stringify(message));
        }
    });
}

/**
 * Debugging purpose
 * @param  {String} data
 * @return {void}
 */
function debug(data) {
    console.log((new Date()) + ' ' + data);
}
