/**
* @class ClientNotification
* @author Stephane Chapdelaine <stephanechapdelaine@gmail.com>
* Simple Chat using WebSocket with custom communication protocol
*/

/**
 * @param {Object} scope will hold the configuration of the class
 * @param {Object} handlers will hold the callbacks handlers when events happens in the websocket
 */
function ClientNotification(scope,handlers) {
    // Configuration values
	this.scope = scope;
    // callbacks for event handlers
    this.datahandlers = handlers;
};

/**
 * Handler for when the connection has been made
 * @return {void}
 */
ClientNotification.prototype.handleWebsocketOpen = function() {
	console.log('Connected to server!');
};

/**
 * Handler for when the connection has been closed
 * @return {void}
 */
ClientNotification.prototype.handleWebsocketClose = function() {
	console.log("WebSocket Connection Closed.");
};

/**
 * Try to execute command based on the data received
 * @param  {JSON String} message
 * @return {void}
 */
ClientNotification.prototype.handleWebsocketMessage = function(message) {
    try {
		//console.log('handleWebsocketMessage',message);
        var command = JSON.parse(message.data);
    }
    catch(e) { console.log(e); }
    
    if (command) {
        this.dispatchCommand(command);
    }
};

/**
 * Function to call to initiate the connection to the websocket server
 * @return {void}
 */
ClientNotification.prototype.connect = function() {
    // Url to request
    var url = this.scope.url || "ws://" + document.URL.substr(7).split('/')[0] + "/ident/1:8080";
	// The protocol to use
    var protocol = this.scope.protocol || "any-protocol";
    
    // Initiate the Browser WebSocket
    var wsCtor = window['MozWebSocket'] ? MozWebSocket : WebSocket;
    this.socket = new wsCtor(url, protocol);

    // Link the events of the socket to our handlers
	this.socket.onopen = this.handleWebsocketOpen.bind(this);
    this.socket.onmessage = this.handleWebsocketMessage.bind(this);
    this.socket.onclose = this.handleWebsocketClose.bind(this);

};

/**
 * Search for handlers of requested command to execute and call it with the data received
 * @param  {JSON Object} command
 * @return {void}
 */
ClientNotification.prototype.dispatchCommand = function(command) {
    // Do we have a handler function for this command?
	console.log('DISPATCH COMMAND',command);
    var datahandler = this.datahandlers["on_"+command.datahandler];
    if (typeof(datahandler) === 'function') {
        // If so, call it and pass the parameter data
       datahandler.call(this, command);
    }
};

/**
 * Allow to send a request to the websocket server to broadcast a command with data to all clients
 * @param  {String} datahandler the name of the handler to use when received by recipients
 * @param  {String} data the message to send
 * @return {void}
 */
ClientNotification.prototype.broadcast = function(datahandler, data) {
    this.socket.send(JSON.stringify({"action":"broadcast","datahandler": datahandler, "data":data }));
};
